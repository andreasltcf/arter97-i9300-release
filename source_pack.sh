#!/bin/bash

tar -Ipixz -pcvf arter97-kernel-$(cat version).tar.xz * --exclude=*.sh --exclude=mkbootimg --exclude=README_f2fs --exclude=ramdisk --exclude=e210 --exclude=kernelzip --exclude=recovery*
